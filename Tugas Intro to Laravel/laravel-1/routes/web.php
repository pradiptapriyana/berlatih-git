<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;

//Route::get('/', 'HomeController@index');

Route::get('/register', 'AuthController@form');

Route::post('/welcome', 'AuthController@hello');

Route::get('/', function() {
    return view('welcome');
});

Route::get('/master', function() {
    return view('AdminLTE.master');
});

Route::get('/items', function() {
    return view('items.index');
});

Route::get('/items/create', function() {
    return view('items.create');
});

// Route::get('/cast', 'CastController@index');
// Route::post('/cast', 'CastController@store');

// Route::get('/cast/create', 'CastController@create');

// Route::get('/cast/{id}', 'CastController@show');
// Route::get('/cast/{id}/edit', 'CastController@edit');

// Route::put('/cast/{id}', 'CastController@update');
// Route::delete('/cast/{id}', 'CastController@destroy');

Route::resource('cast', 'CastController');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');