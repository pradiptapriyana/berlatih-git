<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function form() {
        return view('form');
    } 
    public function hello(Request $request) {
        //dd($request->all());
        $first_name = $request["nama-depan"];
        $last_name = $request["nama-belakang"];
        return view('hello', ['first_name' => $first_name, 'last_name' => $last_name]);
    } 
}
