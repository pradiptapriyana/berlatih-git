<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;

class CastController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }
    
    public function index() {
        // $casts = \DB::table('cast')->get();
        //dd($casts);
        $casts = Cast::all();
        return view('Cast.index', compact('casts'));
    }
    public function create() {
        return view('Cast.create');
    }
    public function store(Request $request) {
        //dd($request->all());   
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        // $query = \DB::table('cast')->insert([
        //     "nama" => $request["nama"],
        //     "umur" => $request["umur"],
        //     "bio" => $request["bio"]
        // ]);

        // $cast = new Cast;
        // $cast->nama = $request["nama"];
        // $cast->umur = $request["umur"];
        // $cast->bio = $request["bio"];
        // $cast->save();

        $cast = Cast::create([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);


        return redirect('/cast')->with('success', 'Cast Berhasil Ditambahkan!');
    }
    public function show($id) {
        // $cast = \DB::table('cast')->where('id',$id)->first();
        $cast = Cast::find($id);
        return view('Cast.show', compact('cast'));
    }
    public function edit($id) {
        // $cast = \DB::table('cast')->where('id',$id)->first();
        $cast = Cast::find($id);
        return view('Cast.edit', compact('cast'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        // $cast = \DB::table('cast')
        //             ->where('id',$id)
        //             ->update([
        //                 'nama' => $request['nama'],
        //                 'umur' => $request['umur'],
        //                 'bio' => $request['bio']
        //             ]);
        $cast = Cast::where('id', $id)->update([
                        'nama' => $request['nama'],
                        'umur' => $request['umur'],
                        'bio' => $request['bio'] 
        ]);
            
        return redirect('/cast')->with('success', 'Berhasil Update Data!');
    }
    public function destroy($id) {
        // $cast = \DB::table('cast')->where('id',$id)->delete();
        $cast = Cast::destroy($id);
        return redirect('/cast')->with('success', 'Data berhasil di-delete!');
    }
}
