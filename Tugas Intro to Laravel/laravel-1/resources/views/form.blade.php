<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="widh=device-width, initial-scale=1.0">
    <title>Index</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label> First Name: </label> <br><br>
        <input type="text" name="nama-depan"> <br><br>
        <label> Last Name: </label> <br><br>
        <input type="text" name="nama-belakang"> <br><br>
        <label> Gender: </label> <br><br>
        <input type="radio" name="male"> Male <br>
        <input type="radio" name="female"> Female <br>
        <input type="radio" name="other"> Other <br> <br>
        <label> Nationality: </label> <br><br>
        <select name="kebangsaan">
            <option value="1">Indonesian</option> <br>
            <option value="2">Singaporean</option> <br>
            <option value="3">Malaysian</option> <br>
            <option value="4">Australian</option> <br>
        </select> <br> <br>
        <label> Language Spoken: </label> <br> <br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br> <br>
        <label> Bio: </label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>