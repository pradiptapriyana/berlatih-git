@extends('AdminLTE.master')

@section('content')
<div class="mt-3 ml-3">
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Daftar Cast Baru</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="{{ route('cast.index')}}" method="POST">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="nama-cast">Nama</label>
          <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', '')}}" placeholder="Nama">
          @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="umur">Umur</label>
          <input type="text" class="form-control" id="umur" name="umur" value="{{ old('umur', '')}}" placeholder="Umur">
          @error('umur')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="bio">Bio</label>
          <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', '')}}" placeholder="Bio">
          @error('bio')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>

      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
@endsection