@extends('AdminLTE.master')

@section('content')
<div class="mt-3 ml-3">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Daftar Cast</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          @if(session('success'))
            <div class="alert alert-success">
                {{ session('success')}}
            </div>
          @endif
          <a class="btn btn-primary mb-3" href="{{ route('cast.create')}}">Tambah Cast baru</a>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th style="width: 40px">Label</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($casts as $key => $cast)
                  <tr>
                      <td> {{ $key + 1 }} </td>
                      <td> {{ $cast -> nama }} </td>
                      <td> {{ $cast -> umur }} </td>
                      <td> {{ $cast -> bio }} </td>
                      <td style="display: flex">  
                        <a href="{{ route('cast.show', ['cast' => $cast->id])}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="{{ route('cast.edit', ['cast' => $cast->id])}}" class="btn btn-default btn-sm">Rubah</a>
                        <form action="{{ route('cast.destroy', ['cast' => $cast->id])}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                      </td>
                  </tr>
              @empty 
                  <tr>
                      <td colspan="5" align="center"> Tidak ada Data </td>
                  </tr>
              @endforelse
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
</div>
@endsection