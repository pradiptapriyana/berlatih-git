<?php 

//require('animal.php');
require('Frog.php');
require('Ape.php');

$sheep =  new Animal("shaun");
echo "RELEASE 0 <br><br>";
echo "$sheep->name <br>"; // "shaun"
echo "$sheep->legs <br>"; // 4
echo "$sheep->cold_blooded <br><br>"; // "no"

echo "RELEASE 1 <br><br>";
echo "Name : $sheep->name <br>";
echo "legs : $sheep->legs <br>";
echo "cold blooded : $sheep->cold_blooded <br><br>";

$frog = new Frog("buduk");
echo "Name : $frog->name <br>";
echo "legs : $frog->legs <br>";
echo "cold blooded : $frog->cold_blooded <br>";
$frog->jump();

$ape = new Ape("kera sakti");
echo "Name : $ape->name <br>";
echo "legs : $ape->legs <br>";
echo "cold blooded : $ape->cold_blooded <br>";
$ape->yell();